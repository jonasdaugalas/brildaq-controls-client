import { NgModule } from '@angular/core';
import { ClarityModule } from 'clarity-angular';

@NgModule({
    declarations: [],
    imports: [
        ClarityModule.forChild()
    ],
    providers: []
})
export class SharedModule { }
