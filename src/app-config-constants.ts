export const WEBLOGREADER_BASE = 'http://cmsrc-lumi.cms:5009/logs';
export const AUTOUPDATE_INTERVAL = 57000;
export const UPDATE_UNSTABLE_STATES_INTERVAL = 5000;
